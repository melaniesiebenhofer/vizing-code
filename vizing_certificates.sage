# ==========================================================================
#   SOS Certificates for Vizing's Conjecture via Gröbner Bases --
#   Compute certificates for k_G = k_H = 1 and given d = n_G + n_H - 1
# --------------------------------------------------------------------------
#   Copyright (C) 2021 Melanie Siebenhofer <melanie.siebenhofer@aau.at>
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see https://www.gnu.org/licenses/.
# ==========================================================================


def get_sdp(d):
    # build symbolic matrix F with F_{ij} = c_i * c_j
    F_temp = [[var('f_%d_%d' % (i,j)) for j in srange(i+1)] for i in srange(ceil(d/2)+1)]
    F =  matrix(SR, ceil(d/2)+1)
    for i in srange(ceil(d/2)+1):
        for j in srange(i):
            F[i,j] = F_temp[i][j]
        for j in srange(i,ceil(d/2)+1):
            F[i,j] = F_temp[j][i]
    # c_0 = -c_1
    F[0,1:] = -F[1,1:]
    F[1:,0] = -F[1:,1]
    F[0,0] = F[1,1]
    
    # build equations
    coeffs = []
    for k in srange(d+1):
        coeff = 0
        for i in srange(ceil(k/2), min(ceil(d/2),k)+1):
            coeff += F[i,i] * binomial(i,k-i) * binomial(k,i)
        for j in srange(ceil((k+1)/2), min(ceil(d/2),k)+1):
            for i in srange(k-j, j):
                coeff += 2*F[i,j]*binomial(i,k-j)*binomial(k,i)
        coeffs.append(coeff)
    coeffs[0] += 1
    coeffs[1] -= 1
    eqs = [coeffs[i] - coeffs[i%2] == 0 for i in srange(len(coeffs))]
    
    variables = []
    for i in srange(1,len(F_temp)):
        for j in srange(1,len(F_temp[i])):
            variables.append(F_temp[i][j])
      
    # solve equation system
    sol = solve(eqs[2:],variables)
    #TODO: check whether there is a solution    
    sol_lhs = list([eq.lhs() for eq in sol[0]])
    sol_rhs = list([eq.rhs() for eq in sol[0]])
    M = F.subs(dict(zip(sol_lhs,sol_rhs)))
    M = M[1:,1:]
    r_variables = list(M.variables())
    
    # extract matrices A_i and M
    A = [matrix(SR,M.nrows()) for _ in srange(len(r_variables))]
    for k in srange(len(r_variables)):
        for i in srange(M.nrows()):
            for j in srange(i+1):
                A[k][i,j] = M[i,j].coefficient(r_variables[k])
                A[k][j,i] = A[k][i,j] 
                M[i,j] -= A[k][i,j]*r_variables[k]
                M[j,i] = M[i,j]
    
    return (A,M)

def get_bounds(A,M):
    A_real = [matrix(RR,Ai) for Ai in A]
    M_real = matrix(RR,M)
    dim = M.nrows()
    p = SemidefiniteProgram()
    x = p.new_variable()
    p.add_constraint(sum([A_real[i]*x[i] for i in srange(len(A))]) + M_real >= matrix.zero(dim,dim,sparse=True))
    p.set_objective(x[0])
    opt = p.solve(objective_only=True)
    a_max = opt
    p.set_objective(-x[0])
    opt = - p.solve(objective_only=True)
    a_min = opt
    return (a_min,a_max)

def find_nice_fraction_in_interval(a,b):
    length = b - a
    if length >= 1: # there is for sure an integer in [a,b]
        return round((a + b)/2)
    else:
        error = length/2 - 10^(-7)*length
        return (RR((a + b)/2)).nearby_rational(max_error=error) 

def find_psd_matrix(A,M):
    while len(A) > 0:
        (a_min,a_max) = get_bounds(A,M)
        a = find_nice_fraction_in_interval(a_min,a_max)
        M += a*A[0]
        A = A[1:]
    return M

def cholesky_naive(F):
    n = F.nrows()
    C = matrix(SR,n)
    C[0,0] = sqrt(F[0,0])
    for i in srange(0,n):
        C[i,0] = F[i,0]/C[0,0]
    for j in srange(1,n):
        C[j,j] = sqrt(F[j,j] - sum([C[j,k]^2 for k in srange(j)]))
        for i in srange(j+1,n):
            C[i,j] = (F[i,j] - sum([C[j,k]*C[i,k] for k in srange(j)]))/C[j,j]
    return C

def find_certificate(d):
    (A,M) = get_sdp(d)
    F = find_psd_matrix(A,M)
    C = cholesky_naive(F)
    return C
